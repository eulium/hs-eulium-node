#!/usr/bin/python3
import paste
from bottle import route, run, template, request, post

import mysql.connector
import datetime
from datetime import datetime

from dbConnector import server
from dbConnector import host
from dbConnector import user
from dbConnector import password
from dbConnector import db
from dbConnector import us_path

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from OpenSSL import crypto, SSL
from socket import gethostname
from pprint import pprint
from time import gmtime, mktime
from itertools import chain, product
import string
import os
import psutil
import random
from Crypto.PublicKey import RSA
from Crypto.Signature.pkcs1_15 import PKCS115_SigScheme
from Crypto.Hash import SHA256
import binascii
from decimal import *
from datetime import datetime
import time
import uuid
import hashlib
import mysql.connector
import random
import json

from flask import Flask, url_for, render_template, session, request, jsonify
from flask_cors import CORS

from flask.json import JSONEncoder
from datetime import date

##################### VERSION 2.0 IMPORTS #####################

from api import getData
from api import syncGlobalDB
from api import validateKeys
from api import updateSoftware
from api import getMyIP
from api import registerHostname
from api import pubkeyAssigned
from api import addRewardKey
from api import createWallet
from api import postTransaction
from api import getBalance
from api import createTransaction
from api import getRewardWalletBalance
from api import getRewardWalletPubkey
from api import postHashBlock
from api import postBlock
from api import updateOnlineStatus
from api import postNode


##################### VERSION 2.0 METHOD #####################
    
app = Flask(__name__)
CORS(app)

##################### VERSION 2.0 METHOD #####################

@app.route('/rewards')
def rewards():
    pubkeyAssigned_status = pubkeyAssigned()
    print ("pubkeyAssigned_status", pubkeyAssigned_status)
    
    if  pubkeyAssigned_status != True:
        return render_template('index.html')
    else:  
        return render_template('assigned.html')

##################### VERSION 2.0 METHOD #####################

@app.route('/assignKey', methods=['POST'])
def assignKey():
    reward_pubkey = request.form['reward_pubkey']
    addRewardKey(reward_pubkey)
    return render_template('assigned.html')

##################### VERSION 2.0 METHOD #####################

@app.route('/ping')
def ping():
    value = {
        "status": "200"
        }
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/validateKeys/pubkey=<pubkey>&prikey=<prikey>')
def function1(pubkey, prikey):
    value = validateKeys(pubkey, prikey)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/getData/query=<query>')
def function2(query):
    value = getData(query)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/syncGlobalDB')
def function3():
    value = syncGlobalDB()
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/updateSoftware')
def function4():
    value = updateSoftware()
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/createWallet')
def function5():
    value = createWallet()
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/createTransaction', methods=['POST'])
def function6():
    sender_pubkey = request.form['sender_pubkey']
    sender_prikey = request.form['sender_prikey']
    receiver_pubkey = request.form['receiver_pubkey']
    amount = request.form['amount']
    gas_amount = request.form['gas_amount']

    value = createTransaction(sender_pubkey, sender_prikey, receiver_pubkey, amount, gas_amount)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/postTransaction', methods=['POST'])
def function7():
    hash = request.form['hash']
    certificate = request.form['certificate']
    sender_pubkey = request.form['sender_pubkey']
    receiver_pubkey = request.form['receiver_pubkey']
    amount = request.form['amount']
    gas_amount = request.form['gas_amount']
    coin_name = request.form['coin_name']
    coin_unit = request.form['coin_unit']
    difficulty = request.form['difficulty']
    created_at = request.form['created_at']
    
    value = postTransaction(hash, certificate, sender_pubkey, receiver_pubkey, amount, gas_amount, coin_name, coin_unit, difficulty, created_at)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/postHashBlock', methods=['POST'])
def function8():
    transaction_hash = request.form['transaction_hash']
    transaction_certificate = request.form['transaction_certificate']
    hash_block = request.form['hash_block']
    
    value = postHashBlock(transaction_hash, transaction_certificate, hash_block)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/getBalance/pubkey=<pubkey>')
def function9(pubkey):
    value = getBalance(pubkey)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/getRewardWalletBalance')
def function10():
    value = getRewardWalletBalance()
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/getRewardWalletPubkey')
def function11():
    value = getRewardWalletPubkey()
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/postBlock', methods=['POST'])
def function12():

    hash = request.form['hash']
    pre_hash = request.form['pre_hash']
    meta = request.form['meta']
    coins_left = request.form['coins_left']
    created_at = request.form['created_at']
    
    value = postBlock(hash, pre_hash, meta, coins_left, created_at)
    return value

##################### VERSION 2.0 METHOD #####################

@app.route('/getMyIP')
def function13():
    
    value = getMyIP()
    
    result = {
        "hostname": value,
        "status": "200"
        }
    return result

##################### VERSION 2.0 METHOD #####################

@app.route('/postNode', methods=['POST'])
def function14():
    hostname = request.form['hostname']
    online_at = request.form['online_at']
    
    result = postNode(hostname, online_at)
    return result

##################### VERSION 2.0 METHOD #####################





#REGISTER HOST ON STARTUP
hostname = getMyIP()
registerHostname(hostname)
updateOnlineStatus(hostname)

app.run(host=server, port=80, use_reloader=True, use_debugger=False)