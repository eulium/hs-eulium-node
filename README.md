# Eulium Node Application Version 1.0 #

Eulium a layer-1 blockchain provides a marketplace for scientific research, collaboration, and funding.

### How to install and setup it? ###

1. <b>Create Linux Server</b>

    <br />
    Supproted versions:
    Ubuntu Server 18.04
    <br />
    Ubuntu Server 20.04

    <br />
    List of the most popular cloud computing providers:
    ```
    https://www.digitalocean.com
    ```
    ```
    https://azure.microsoft.com
    ```
    ```
    https://aws.amazon.com
    ```
    ```
    https://cloud.google.com
    ```

    <br />

    Note: The beauty of Eulium blockchain is that you can earn ELM coins (getting rewards for mining and transactions verification) with no significant requirements of the computing power. Because the chain is designed based on Proof of Stake (PoS) instead of Proof of Work (PoW), your mining server will not need to resolve complex mathematical tasks to mine Eulium blocks. Because of this, minimal server requirements a pretty low: vCPU 1, RAM 1 GiB, HDD 10 GiB.

2. <b>Open Port</b>

    Make sure port 80 opens on your Linux server. Eulium installation script will try to open the port automatically but depends on your cloud computing provider, you might need to do this manually. Please read the instructions posted by your cloud computing provider.

3. <b>Install Eulium Node App</b>

    You can install recent Eulium Node App for Linux via run this shell command in terminal of your server:

    ```
    sudo git -C / clone https://klimanov@bitbucket.org/eulium/hs-eulium-node.git ; sudo chmod +x /hs-eulium-node/deployment/script.sh ; sudo /hs-eulium-node/deployment/script.sh
    ```

    Note: It may take a few minutes until all dependencies and application are installed on your server. The server will be automatically rebooted after installation is complete.

4. <b>Check Access to App</b>

    Make sure Eulium Node App installed correctly and your server is online.
    Load this webpage in your browser:
    ```
    http://YOUR_SERVER_PUBLIC_IP_ADDRESS/ping
    ```

    You should see this response:
    ```
    {"status":"200"}
    ```
    
    If so, congratulations! You are successfully installed your Eulium Node Application!


5. <b>Create New Wallet</b>

    If you don't have your Eulium Wallet yet, it can be easily created via loading this webpage in your browser:
    ```
    http://YOUR_SERVER_PUBLIC_IP_ADDRESS/createWallet
    ```

    Important! The chain never store private keys of wallets. Please ensure you store your Eulium Wallet credentials (Address, Public key, Private key) in a secure place. If you forget the combination of your wallet public-private keys, you have no ability to operate with your funds anymore.

    Please be aware! You can assign the Eulium wallet public key to the Server only once for security purposes. If you want to use another public key, you must re-install Eulium Node App. To do that, go back to Step 3 and run the shell command in your terminal again. 


6. <b>Assign Wallet to Server</b>

    The last and essential step is to assign your Eulium Wallet Public Key to your Server. The assigned wallet collects all rewards that your Node generates.
    Load this webpage in your browser:
    ```
    http://YOUR_SERVER_PUBLIC_IP_ADDRESS/rewards
    ```

    Note! Only the positive balance of the assigned wallet allows the node to generate rewards out of transactions validation.
    

### Eulium Node App API ###

1. <b>Startup Endpoints</b>
    <br />
    You can use any of the following startup endpoints to execute API methods or any other endpoints (public IPs) of any Eulium Node App Server:</b>
    ```
    http://node1.eulium.org/
    ```
    ```
    http://node2.eulium.org/
    ```
    ```
    http://node3.eulium.org/
    ```
    ```
    http://node4.eulium.org/
    ```
    ```
    http://node5.eulium.org/
    ```

2. <b>API Methods</b>

    Create Wallet [GET]
    ```
    http://ENDPOINT/createWallet
    ```
<br />
    Check Wallet Balance [GET]
    ```
    http://ENDPOINT/getBalance/pubkey=YOUR_WALLET_PUBLIC_KEY
    ```
<br />
    Create transaction [POST]
    ```
    http://ENDPOINT/createTransaction

    #POST requests values
    sender_pubkey = 'sender_pubkey'
    sender_prikey = 'sender_prikey'
    receiver_pubkey = 'receiver_pubkey'
    amount = 'amount'
    gas_amount = 'gas_amount'

    #Note: POST requests are used to send data to the Eulium Node Server. The data sent to the server is stored in the request body of the HTTP request.
    ```
<br />
    Get Data (nodes) [GET]
    ```
    http://ENDPOINT/getData/query=*%20FROM%20nodes
    ```
<br />
    Get Data (wallets) [GET]
    ```
    http://ENDPOINT/getData/query=*%20FROM%20wallets
    ```
<br />
    Get Data (transactions) [GET]
    ```
    http://ENDPOINT/getData/query=*%20FROM%20transactions
    ```
<br />
    Get Data (blocks) [GET]
    ```
    http://ENDPOINT/getData/query=*%20FROM%20blocks
    ```
<br />
    Update Software [GET]
    ```
    http://ENDPOINT/updateSoftware

    #After executing this method Node App Software will be updated on the background.
    #Execution may take a few seconds. If and when software successfully updated {"status":"200"} should be returned.
    ```
<br />
    Assign Reward Wallet [GET]
    ```
    http://ENDPOINT/rewards

    #Reward Wallet public key can be assigned to the Node App Server only once. 
    ```

### Rewards Distribution ###

Eulium chain is designed to provide two types of rewards to the community of miners.

1. <b>Reward for transaction validation</b>

    Each Eulium Node Server with an assigned Wallet Public Key generates a reward from each created transaction in an entire chain. If Node is online, it automatically validates all new transactions and gets a portion out of the transaction gas amount. Portions of reward are calculated individually for each Node based on the amount of ELM coins allocated in the assigned Wallet. The Node with the highest balance of assigned Wallet will get a proportionally higher reward per transaction.

    <br />

    Reward calculation Example:
    ```
    SenderA sent 1000ELM to ReceiverB. SenderA declared the max gas amount for a particular transaction 10ELM

    online node1 with assigned wallet balance 100ELM
    online node2 with assigned wallet balance 120ELM
    online node3 with assigned wallet balance 200ELM
    
    Total balance of all available Nodes 420ELM
    Total gas amount 10ELM
    
    node1 will get reward ( 100 / 420 ) * 10 = 2.38... ELM
    node2 will get reward ( 120 / 420 ) * 10 = 2.85... ELM
    node3 will get reward ( 200 / 420 ) * 10 = 4.76... ELM
    ```

    <br />

2. <b>Reward for accessibility</b>

    Each Eulium Node Server with an assigned Wallet Public Key gets a reward amount every 24 hours from Eulium Daily Reward Pool. (Lifetime Reward Pool amount is 500,000,000ELM) If Node is online for 24 hours, it automatically grants a portion out of the Eulium Daily Reward Pool: 10,000 ELM per day for all active Nodes in the chain. Every day this amount is automatically and proportionally distributed to all available Nodes.

    Reward calculation Example:
    ```
    Eulium Daily Reward Pool 10000ELM
    Number of online Nodes 1000

    Every 24 hours each online Node will be rewarded:
    node1 will get reward ( 10000 / 1000 = 10ELM )
    node2 will get reward ( 10000 / 1000 = 10ELM )
    node3 will get reward ( 10000 / 1000 = 10ELM )
    ...
    node1000 will get reward ( 10000 / 1000 = 10ELM )
    ```


### Who do I talk to? ###

    https://eulium.org/support
    