#!/bin/bash  

echo "Thank you for installing Eulium Node Application for Linux 18"  

# Description: Set up Netwoerk environment
echo | sudo apt update
echo | sudo apt upgrade -y

echo | sudo ufw allow from any to any port 80 proto tcp
echo | sudo ufw allow from any to any port 443 proto tcp

# Description: Set up Python3 requared libraries
echo | sudo apt install python3.11 -y
echo | sudo apt-get -y install python3-pip
#echo | sudo apt install python3-pip -y
echo | sudo rm /usr/lib/python3.11/EXTERNALLY-MANAGED
echo | sudo pip3 install paste
echo | sudo pip3 install bottle
echo | sudo pip3 install mysql-connector-python
echo | sudo pip3 install --upgrade psutil
echo | sudo pip3 install pycryptodome

#echo | sudo pip3 install flask
echo | sudo pip3 install Flask==2.2.3
echo | sudo pip3 install Werkzeug==2.2.2
echo | sudo pip3 install -U flask-cors
echo | sudo pip3 install simplejson
echo | sudo pip3 install schedule
echo | sudo pip3 install cryptography
echo | sudo pip3 install pyOpenSSL
echo | sudo pip3 install --upgrade psutil
echo | sudo pip3 install pycryptodome
echo | sudo pip3 install requests

# Description: Set up MySQL Community Release 5.7
echo | sudo debconf-set-selections <<< 'mysql-server-5.1 mysql-server/root_password password eulium_node'
echo | sudo debconf-set-selections <<< 'mysql-server-5.1 mysql-server/root_password_again password eulium_node'
echo | sudo apt-get -y install mysql-server

# Description: Set up components
echo | sudo python3 /hs-eulium-node/setup.py

# Description: Set up crontab task
echo | cat <(crontab -l) <(echo "@reboot sudo python3 /hs-eulium-node-startup/startup.py") | crontab -

echo | cat <(crontab -l) <(echo "@reboot sleep 10 && sudo python3 /hs-eulium-node/syncNodes.py") | crontab -
echo | cat <(crontab -l) <(echo "@reboot sleep 11 && sudo python3 /hs-eulium-node/syncWallets.py") | crontab -
echo | cat <(crontab -l) <(echo "@reboot sleep 12 && sudo python3 /hs-eulium-node/syncTransactions.py") | crontab -

echo | cat <(crontab -l) <(echo "@reboot sleep 13 && sudo python3 /hs-eulium-node/verifyTransaction.py") | crontab -
echo | cat <(crontab -l) <(echo "@reboot sleep 14 && sudo python3 /hs-eulium-node/mineBlock.py") | crontab -
echo | cat <(crontab -l) <(echo "@reboot sleep 15 && sudo python3 /hs-eulium-node/mineReward.py") | crontab -

echo | cat <(crontab -l) <(echo "@reboot sleep 16 && sudo python3 /hs-eulium-node/checkIntegrity.py") | crontab -

# Description: Reboot machine
echo | sudo reboot