import urllib.request

#VERSION 2.0
host = "127.0.0.1"
user = "root"
password = "eulium_node"
db = "sys"

#SERVER
server = "0.0.0.0"

#SQL PATH (SERVER)
global_db_path = "/hs-eulium-node/database/global.sql"
local_db_path = "/hs-eulium-node/database/local.sql"
app_path = "sudo python3 /hs-eulium-node/app.py"
us_path = "sudo python3 /hs-eulium-node/updateSoftware.py"

#STARTUP SQL PATH (DEBAGGING)
#global_db_path = "database/global.sql"
#local_db_path = "database/local.sql"
#app_path = "sudo python3 app.py"
#us_path = "sudo python3 updateSoftware.py"
#path = "/hs-eulium-node/"