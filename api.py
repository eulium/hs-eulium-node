import datetime
from datetime import datetime

from dbConnector import host
from dbConnector import user
from dbConnector import password
from dbConnector import db

from dbConnector import global_db_path
from dbConnector import local_db_path

from dbConnector import server

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

from OpenSSL import crypto, SSL
from socket import gethostname
from pprint import pprint
from time import gmtime, mktime

from itertools import chain, product
import string
import os
import psutil
import random

from Crypto.PublicKey import RSA
from Crypto.Signature.pkcs1_15 import PKCS115_SigScheme
from Crypto.Hash import SHA256
import binascii

from decimal import *
from datetime import datetime
import time
import uuid
import hashlib
import mysql.connector
import random

import simplejson as json

from flask import Flask, jsonify
import urllib.request
from urllib.request import urlopen

import requests
from collections import Counter 

import sys
import hashlib


##################### VERSION 2.0 METHOD #####################

def getSystemUtilizationStats():
    ram = psutil.virtual_memory().percent
    cpu = psutil.cpu_percent()
    disk = psutil.disk_usage(os.sep).percent
    print ("ram usage (%):", ram)
    print ("cpu usage (%):", cpu)
    print ("disk usage (%):", disk)
    return

##################### VERSION 2.0 METHOD #####################

def getData(query):
    try:
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        mycursor.execute("USE global")
        mycursor.execute("SELECT "+query)
        row_headers=[x[0] for x in mycursor.description]
        result = mycursor.fetchall()
        json_data=[]
        for item in result:
            json_data.append(dict(zip(row_headers,item)))
            
        mydb.close()
        
        result = {
            "data": json_data,
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result

##################### VERSION 2.0 METHOD #####################

def syncGlobalDB():
    try:
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        fd = open(global_db_path, 'r')
        sqlFile = fd.read()
        fd.close()
        sqlCommands = sqlFile.split(';')
        for command in sqlCommands:
            try:
                if command.strip() != '':
                    mycursor.execute(command)
                    print (command)
            except:
                print ("Command skipped")
                
        mydb.close()
        result = {
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result

##################### VERSION 2.0 METHOD #####################

def syncLocalDB():
    try:
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        fd = open(local_db_path, 'r')
        sqlFile = fd.read()
        fd.close()
        sqlCommands = sqlFile.split(';')
        for command in sqlCommands:
            try:
                if command.strip() != '':
                    mycursor.execute(command)
                    print (command)
            except:
                print ("Command skipped")
        mydb.close()
        result = {
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result

##################### VERSION 2.0 METHOD #####################

def createWallet():
    try:
        letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        pubkey = ( ''.join(random.choice(letters) for i in range(60)) )
        letters_address = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        address_start = "ELM@"
        address_base = ( ''.join(random.choice(letters_address) for i in range(30)) )
        address = address_start + address_base
        created_at = round(time.time() * 1000)
        
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        mycursor.execute("USE global")
        sql = "INSERT INTO wallets (pubkey, address, created_at) VALUES (%s, %s, %s)"
        val = (pubkey, address, created_at)
        mycursor.execute(sql, val)
        mydb.commit()
        
        mydb.close()
        
        # uuid is used to generate a random number
        salt = uuid.uuid4().hex
        prikey = hashlib.sha256(salt.encode() + pubkey.encode()).hexdigest() + ':' + salt
        print ("pubkey", pubkey)
        print ("prikey", prikey)
        
        data = {
            "address": address,
            "pubkey": pubkey,
            "prikey": prikey,
            "current_balance": "0.00",
            "coin_name": "Eulium",
            "coin_unit": "ELM"
        }
        result = {
            "data": data,
            "status": "200"
            }
    except:
        result = {
            "status": "404"
        }
    return result
    
##################### VERSION 2.0 METHOD #####################
        
def getMyIP():
    try:
        result = urllib.request.urlopen('http://ident.me').read().decode('utf8')   
    except:
        result = False
    return result
        
##################### VERSION 2.0 METHOD #####################        

def getNodes():
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    #ONLY RECENTLY ONINE NODES WILL BE RETURNED
    now = round(time.time() * 1000)
    delta = now - (24*60*60*1000) #24 HOURS BACK
    delta = str(delta)
    
    mycursor.execute("USE global")
    mycursor.execute("SELECT hostname FROM nodes WHERE online_at > '"+delta+"'")
    result = mycursor.fetchall()
    
    #GET IP ADDRESS OF STARTUP NODES BY STATIC URLS AND ADD STARTUP NODES TO ALL REQUESTS

    try:
        data_json = requests.get("http://node1.eulium.org/getMyIP", verify=False, timeout=5).json()
        hostname_node1 = data_json["hostname"]
        if (hostname_node1 not in result):
            result.append([hostname_node1])
    except:
        print("Can't ping Node1")

    try:
        data_json = requests.get("http://node2.eulium.org/getMyIP", verify=False, timeout=5).json()
        hostname_node2 = data_json["hostname"]
        if (hostname_node2 not in result):
            result.append([hostname_node2])
    except:
        print("Can't ping Node2")

    try:
        data_json = requests.get("http://node3.eulium.org/getMyIP", verify=False, timeout=5).json()
        hostname_node3 = data_json["hostname"]
        if (hostname_node3 not in result):
            result.append([hostname_node3])
    except:
        print("Can't ping Node3")
    
    try:
        data_json = requests.get("http://node4.eulium.org/getMyIP", verify=False, timeout=5).json()
        hostname_node4 = data_json["hostname"]
        if (hostname_node4 not in result):
            result.append([hostname_node4])
    except:
        print("Can't ping Node4")
    
    try:
        data_json = requests.get("http://node5.eulium.org/getMyIP", verify=False, timeout=5).json()
        hostname_node5 = data_json["hostname"]
        if (hostname_node5 not in result):
            result.append([hostname_node5])
    except:
        print("Can't ping Node5")
    
    print ("list of all available nodes", result)
    
    mydb.close()
    return result

##################### VERSION 2.0 METHOD #####################

def getWallets():
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    mycursor.execute("USE global")
    mycursor.execute("SELECT pubkey FROM wallets")
    result = mycursor.fetchall()
    
    mydb.close()
    return result

##################### VERSION 2.0 METHOD #####################

def checkIfNodeInMyList(hostname):
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    hostname = str(hostname)
    
    mycursor.execute("USE global")
    mycursor.execute("SELECT id FROM nodes WHERE hostname LIKE '"+hostname+"'")
    result = mycursor.fetchall()
    mydb.close()
    
    if not result:
        result = False
    else:
        result = True
    print ("checkIfNodeInMyList", result)
    return result

##################### VERSION 2.0 METHOD #####################

def checkIfWalletInMyList(pubkey):
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    mycursor.execute("USE global")
    mycursor.execute("SELECT id FROM wallets WHERE pubkey LIKE '"+pubkey+"'")
    result = mycursor.fetchall()
    mydb.close()
    if not result:
        result = False
    else:
        result = True
    return result

##################### VERSION 2.0 METHOD #####################

def checkIfTransactionInMyList(certificate):
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    certificate = str(certificate)
    
    mycursor.execute("USE global")
    mycursor.execute("SELECT id FROM transactions WHERE certificate LIKE '"+certificate+"'")
    result = mycursor.fetchall()
    mydb.close()
    
    if not result:
        result = False
    else:
        result = True
    return result

##################### VERSION 2.0 METHOD #####################

def validateKeys(pubkey, prikey):
    try:
        password, salt = prikey.split(':')
        response = password == hashlib.sha256(salt.encode() + pubkey.encode()).hexdigest()
        if response == True:
            return True
        else:
            return False
    except:
        return False

##################### VERSION 2.0 METHOD #####################

def updateSoftware():
    try:
        try:
            os.system("sudo rm -rf /hs-eulium-node")
        except:
            pass
        
        os.system("sudo git -C / clone https://klimanov@bitbucket.org/eulium/hs-eulium-node.git")
        time.sleep(5)
        
        print ("Software updated")
        result = {
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result
    
##################### VERSION 2.0 METHOD #####################
    
def registerHostname(hostname):

    checkIfNodeInMyList_status = checkIfNodeInMyList(hostname)
    if checkIfNodeInMyList_status == False:
    
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        mycursor.execute("USE global")
        sql = "INSERT INTO nodes (hostname) VALUES (%s)"
        val = (hostname,)
        mycursor.execute(sql, val)
        mydb.commit()
        
        mydb.close()
        
        print("IP address:", hostname, "sucessfully registered")
        
    else:
        
        print("IP address:", hostname, "alredy registered")

    return

##################### VERSION 2.0 METHOD #####################

def pubkeyAssigned():
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    mycursor.execute("USE local")
    mycursor.execute("SELECT COUNT(id) FROM settings")
    result = mycursor.fetchall()

    for item in result:
        count = item[0]
    
    mydb.close()
    
    if count == 0:
        result = False
    else:
        result = True
    return result

##################### VERSION 2.0 METHOD #####################

def addRewardKey(reward_pubkey):
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    assigned_at = round(time.time() * 1000)
    mycursor.execute("USE local")
    sql = "INSERT INTO settings (reward_pubkey, assigned_at) VALUES (%s, %s)"
    val = (reward_pubkey, assigned_at)
    mycursor.execute(sql, val)
    mydb.commit()
    
    mydb.close()
    print("Public Key Assigned")
    return True

##################### VERSION 2.0 METHOD #####################

def syncNodes():
    
    my_hostname = getMyIP()
    print ("my_hostname", my_hostname)
    
    #UPDATE ONLINE STATUS FOR THIS NODE
    updateOnlineStatus(my_hostname)
    
    #POST MY HOSTNAME TO ALL NODES IN MY LIST
    result = getNodes()
    for item in result:
        hostname = item[0]
        
        online_at = round(time.time() * 1000)
        
        url_post = "http://"+hostname+"/postNode"
        data_post = {'hostname': my_hostname, 'online_at': online_at}
        print ("data_post", data_post)

        try:
            requests.post(url_post, data_post, timeout=5)
            print(hostname, "Node data sucessfully posted to", url_post)
        except:
            print ("Can't ping URL", url_post)

    #GET HOSTNAMES FROM ALL NODES IN MY LIST
    result = getNodes()
    for item in result:
        hostname = item[0]

        url = "http://"+hostname+"/getData/query=*%20FROM%20nodes"
        print(url)
        try:
            data_json = requests.get(url, verify=False, timeout=5).json()
        except:
            print(hostname, "Can't ping URL")
            continue

        for row in data_json['data']:
            hostname = row["hostname"]
            online_at = row["online_at"]
            
            #CHECK IF hostname EXIST IN LOCAL NODE LIST, IF NOT ADD
            checkIfNodeInMyList_status = checkIfNodeInMyList(hostname)
            
            if checkIfNodeInMyList_status == False:

                mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
                mycursor = mydb.cursor()
                
                mycursor.execute("USE global")
                sql = "INSERT INTO nodes (hostname, online_at) VALUES (%s, %s)"
                val = (hostname, online_at)
                mycursor.execute(sql, val)
                mydb.commit()
                mydb.close()
                
                print(hostname, "Node added")
            
            else:
                mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
                mycursor = mydb.cursor()
                
                mycursor.execute("USE global")
                mycursor.execute("""UPDATE nodes SET online_at=%s WHERE hostname=%s""", (online_at, hostname, ))
                mydb.commit()
                mydb.close()
                
                print(hostname, "Node updated")
         
    #DELETE DUBLICATED NODES IF EXIST
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    mycursor.execute("USE global")
    mycursor.execute("DELETE S1 FROM nodes AS S1 INNER JOIN nodes AS S2 WHERE S1.id < S2.id AND S1.hostname = S2.hostname")
    mydb.close()
    return True

##################### VERSION 2.0 METHOD #####################

def syncWallets():

    result = getNodes()
    for item in result:
        hostname = item[0]
        url = "http://"+hostname+"/getData/query=*%20FROM%20wallets"
        print(url)
        try:
            data_json = requests.get(url, verify=False, timeout=5).json()
        except:
            print("Can't open URL")
            continue

        for row in data_json['data']:
            pubkey = row["pubkey"]
            address = row["address"]
            created_at = row["created_at"]
            
            #CHECK IF hostname EXIST IN LOCAL WALLET LIST, IF NOT ADD
            checkIfWalletInMyList_status = checkIfWalletInMyList(pubkey)
            if checkIfWalletInMyList_status == False:
                mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
                mycursor = mydb.cursor()
                mycursor.execute("USE global")
                sql = "INSERT INTO wallets (pubkey, address, created_at) VALUES (%s, %s, %s)"
                val = (pubkey, address, created_at)
                mycursor.execute(sql, val)
                mydb.commit()
                mydb.close()
                print(pubkey, "added")
            else:
                print(pubkey, "exist")
                pass
            
    #DELETE DUBLICATED WALLETS IF EXIST
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    mycursor.execute("USE global")
    mycursor.execute("DELETE S1 FROM wallets AS S1 INNER JOIN wallets AS S2 WHERE S1.id < S2.id AND S1.pubkey = S2.pubkey")
    mydb.close()

    return True

##################### VERSION 2.0 METHOD #####################

def syncTransactions():
    
    result = getNodes()
    for item in result:
        hostname = item[0]
        url = "http://"+hostname+"/getData/query=* FROM transactions ORDER BY created_at ASC"
        print(url)
        
        try:
            data_json = requests.get(url, verify=False, timeout=5).json()
        except:
            print(hostname, "Can't open URL")
            continue

        for row in data_json['data']:
            hash = row["hash"]
            certificate = row["certificate"]
            sender_pubkey = row["sender_pubkey"]
            receiver_pubkey = row["receiver_pubkey"]
            amount = row["amount"]
            gas_amount = row["gas_amount"]
            coin_name = row["coin_name"]
            coin_unit = row["coin_unit"]
            difficulty = row["difficulty"]
            created_at = row["created_at"]
            verified_at = row["verified_at"]
            execution_time = row["execution_time"]
            rejected_at = row["rejected_at"]
            hash_block = row["hash_block"]
            
            #CHECK IF TRANSACTION EXIST IN LOCAL TABLE
            checkIfTransactionInMyList_status = checkIfTransactionInMyList(certificate)

            if checkIfTransactionInMyList_status == False:
                mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
                mycursor = mydb.cursor()
                mycursor.execute("USE global")
                
                #SHRINK TRANSACTION HASH BECAUSE EACH NODES HAVE TO VERIFY EACH TRANSACTION INDIVIDUALLY
                open = 40 - int(difficulty)
                hash_short = hash[:open]
                print ("hash_short", hash_short)
            
                sql = "INSERT INTO transactions (hash, certificate, sender_pubkey, receiver_pubkey, amount, gas_amount, coin_name, coin_unit, difficulty, created_at, rejected_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (hash_short, certificate, sender_pubkey, receiver_pubkey, amount, gas_amount, coin_name, coin_unit, difficulty, created_at, rejected_at)
                mycursor.execute(sql, val)
                mydb.commit()
                
                mydb.close()
                print(hostname, "Raw transaction added")
            else:
                print(hostname, "Transaction exist")


    #DELETE DUBLICATED TRANSACTIONS IF EXIST
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    mycursor.execute("USE global")
    mycursor.execute("DELETE S1 FROM transactions AS S1 INNER JOIN transactions AS S2 WHERE S1.id < S2.id AND S1.certificate = S2.certificate")
    mydb.close()
    
    return True

##################### VERSION 2.0 METHOD #####################

def createTransaction(sender_pubkey, sender_prikey, receiver_pubkey, amount, gas_amount):
    
    #CHECK IF RECEVIER WALLET EXIST
    checkIfWalletInMyList_status = checkIfWalletInMyList(receiver_pubkey)
    if checkIfWalletInMyList_status == True:
        
        #MATCH KEYS
        authorization_status = validateKeys(sender_pubkey, sender_prikey)
        if authorization_status == True:
        
            #CHECK BALANCE
            getBalance_json = getBalance(sender_pubkey)
            data_json = getBalance_json['data']
            balance = data_json['current_balance']
            print ("current_balance", balance)

            gas_amount = float(gas_amount)
            amount = float(amount)
            amount_total = amount + gas_amount
            
            if balance >= amount_total:
                
                active_nodes_dict = {}
                coin_name = "Eulium"
                coin_unit = "ELM"
                difficulty = 5
                certificate, hash = generateHashCertificatePair(difficulty) 

                print ('hash: ', hash)
                print ('certificate: ', certificate)
                print ('sender_pubkey: ', sender_pubkey)
                print ('receiver_pubkey', receiver_pubkey)
                print ('difficulty', difficulty)
                print ('amount', amount)
                print ('gas_amount', gas_amount)
                
                #POST NEW CLEINT TRANSACTION TO ALL AVAILABLE NODES INCLUDING THIS ONE
                created_at = round(time.time() * 1000)
                
                result = getNodes()
                for item in result:
                    hostname = item[0]
                    url = "http://"+hostname+"/postTransaction"
                    data = {'hash': hash, 'certificate': certificate, 'sender_pubkey': sender_pubkey, 'receiver_pubkey': receiver_pubkey, 'amount': amount, 'gas_amount': gas_amount, 'coin_name': coin_name, 'coin_unit': coin_unit, 'difficulty': difficulty, 'created_at': created_at}
                    
                    try:
                        #IF NODE IS ACTIVE WE NEED TO REQUEST REWARD WALLET BALANCE FROM IT TO CALCULATE REWARD
                        url_balance = "http://"+hostname+"/getRewardWalletBalance"
                        #response = urlopen(url_balance, timeout=5)
                        #getRewardWalletBalance_json = json.loads(response.read())
                        getRewardWalletBalance_json = requests.get(url_balance, verify=False, timeout=5).json()
                        
                        reward_balance_json = getRewardWalletBalance_json['data']
                        reward_balance = reward_balance_json['current_balance']
                        print (hostname, "reward_balance", reward_balance)
                    except:
                        reward_balance = float(0)
                        print(hostname, "Reward balance", reward_balance)
                    
                    try:
                        #IF WALLET BALANCE AND PUBKEY ARE RECEIVED WE CAN POST TRANSACTION
                        requests.post(url, data, timeout=5)
                        print(hostname, "Main transaction successfully posted to", url)
                        
                        #UPDATE ONLINE STATUS FOR THIS NODE
                        updateOnlineStatus(hostname)
                        print (hostname, "Online status updated")
                        
                    except:
                        print(hostname, "Node is not responding", url)
                        continue
                    
                    #COLLECT ACTIVE HOSTS AND THEIR WALLETS BALANCE TO POST REWARD TRANSACTIONS
                    active_nodes_dict.update({ hostname : reward_balance })

                print ("SUCCESS: New transaction sucessfully posted to available nodes")
                
                #AFTER TRANSACTION POSTED, THE REWARD SHOULD BE CALCULATED AND POSTED AS A SEPARATE TRANSACTIONS PER EACH NODE
                print ("active_nodes_dict", active_nodes_dict)
                
                #COUNT NUMBER OF ACTIVE NODES (REWARD RECEIVERS)
                active_nodes_count = len(active_nodes_dict.keys())
                print ("active_nodes_count", active_nodes_count)
                
                #GET TOTAL BALANCE VALUES OF ALL ACTIVE NODES
                total_balance_value = sum(active_nodes_dict.values())
                print ("total_balance_value", total_balance_value)
                
                #SORT NODES FROM HEIGHEST BALANCE AMOUNT TO LOWEST
                active_nodes_dict_sorted = {}
                sorted_keys = sorted(active_nodes_dict, key=active_nodes_dict.get, reverse=True)
                for w in sorted_keys:
                    active_nodes_dict_sorted[w] = active_nodes_dict[w]
                print ("active_nodes_dict_sorted", active_nodes_dict_sorted)
                
                for active_node_host, active_node_host_balance in active_nodes_dict_sorted.items():
                    
                    print(active_node_host, "active_node_host", active_node_host)
                    print(active_node_host, "active_node_host_balance", active_node_host_balance)
                    print(active_node_host, "gas_amount", gas_amount)
                    
                    #IF REWARD WALLET BALANCE IS ZERO REWARD TRANSACTION WILL NOT BE POSTED
                    if active_node_host_balance == 0:
                        print (active_node_host, "balance is zero, no reward will be posted for this node")
                        continue

                    #REWARD CALCULATION FORMULA FOR PARTICULAR NODE (PROOF OF STAKE)
                    
                    amount_reward = gas_amount / ( total_balance_value / active_node_host_balance )
                    
                    certificate_reward, hash_reward = generateHashCertificatePair(difficulty)
                    
                    gas_amount_reward = float(0)
                    created_at = round(time.time() * 1000)
                    
                    try:
                        #NEED TO REQUEST REWARD WALLET PUBKEY FROM NODE TO CREATE REWARD TRANSACTION
                        url_pubkey = "http://"+active_node_host+"/getRewardWalletPubkey"
                        #response = urlopen(url_pubkey, timeout=5)
                        #getRewardWalletPubkey_json = json.loads(response.read())
                        getRewardWalletPubkey_json = requests.get(url_pubkey, verify=False, timeout=5).json()
                        
                        reward_balance_json = getRewardWalletPubkey_json['data']
                        reward_pubkey = reward_balance_json['pubkey']
                        print (active_node_host, "reward_pubkey", reward_pubkey)
                    except:
                        #IF REWARD WALLET PUBKEY IS NOT AVAILABLE REWARD TRANSACTION WILL NOT BE POSTED
                        reward_pubkey = None
                        print (active_node_host, "reward wallet is not assigned, no reward will be posted for this node")
                        continue
                    
                    #EXAMPLE
                    #node1 balance 100ELM
                    #node2 balance 120ELM
                    #node3 balance 200ELM
                    
                    #Total balance values 420ELM
                    #Total gas amount 10ELMs
                    
                    #node1 reward ( 100 / 420 ) * 10 = 2.38... ELM
                    #node2 reward ( 120 / 420 ) * 10 = 2.85... ELM
                    #node3 reward ( 200 / 420 ) * 10 = 4.76... ELM
                     
                    print (active_node_host, "hash_reward", hash_reward)
                    print (active_node_host, "certificate_reward", certificate_reward)
                    print (active_node_host, "sender_pubkey", sender_pubkey)
                    print (active_node_host, "reward_pubkey", reward_pubkey)
                    print (active_node_host, "amount_reward", amount_reward)
                    print (active_node_host, "gas_amount_reward", gas_amount_reward)
                    print (active_node_host, "created_at", created_at)
                
                    result = getNodes()
                    for item in result:
                        hostname = item[0]
                        url_reward = "http://"+hostname+"/postTransaction"
                        data_reward = {'hash': hash_reward, 'certificate': certificate_reward, 'sender_pubkey': sender_pubkey, 'receiver_pubkey': reward_pubkey, 'amount': amount_reward, 'gas_amount': gas_amount_reward, 'coin_name': coin_name, 'coin_unit': coin_unit, 'difficulty': difficulty, 'created_at': created_at}
                        try:
                            requests.post(url_reward, data_reward, timeout=5)
                            print("Reward transaction successfully posted to", url_reward)
                        except:
                            print("Can't open URL", url_reward)
                            continue
                        
                data = {
                    "coin_unit": "ELM",
                    "transaction_hash": hash,
                    "transaction_certificate": certificate,
                    "sender_pubkey": sender_pubkey,
                    "receiver_pubkey": receiver_pubkey,
                    "transaction_amount": float (amount),
                    "gas_amount": float (gas_amount),
                    "verification_status": "pending"
                    }
                
                result = {
                    "data": data,
                    "status": "200"
                    }
                return result
            else:
                print ("DECLINE: Not enough funds")
                result = {
                    "status": "404"
                    }
                return result
        else:
            print ("DECLINE: Not authorized")
            result = {
                "status": "404"
                }
            return result
    else:
        print ("DECLINE: Receiver wallet not exist")
        result = {
            "status": "404"
            }
        return result

##################### VERSION 2.0 METHOD #####################

def postTransaction(hash, certificate, sender_pubkey_id, receiver_pubkey_id, amount, gas_amount, coin_name, coin_unit, difficulty, created_at):
    
    #CHECK IF TRANSACTION EXIST IN LOCAL TABLE
    checkIfTransactionInMyList_status = checkIfTransactionInMyList(certificate)

    if checkIfTransactionInMyList_status == False:
    
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        sql = "INSERT INTO transactions (hash, certificate, sender_pubkey, receiver_pubkey, amount, gas_amount, coin_name, coin_unit, difficulty, created_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (hash, certificate, sender_pubkey_id, receiver_pubkey_id, amount, gas_amount, coin_name, coin_unit, difficulty, created_at)
        mycursor.execute("USE global")
        mycursor.execute(sql, val)
        mydb.commit()
        
        mydb.close()
    else:
        pass
    
    result = {
        "status": "200"
        }
    return result

##################### VERSION 2.0 METHOD #####################

def postNode(hostname, online_at):
    
    checkIfNodeInMyList_status = checkIfNodeInMyList(hostname)
    if checkIfNodeInMyList_status == False:
        
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        sql = "INSERT INTO nodes (hostname, online_at) VALUES (%s, %s)"
        val = (hostname, online_at)
        mycursor.execute("USE global")
        mycursor.execute(sql, val)
        mydb.commit()
        
        mydb.close()
        
    else:
        #IF NODE EXIST ON LOCAL TABLE UPDATE(SYNC) ONLINE STATUS
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        mycursor.execute("USE global")
        mycursor.execute("""UPDATE nodes SET online_at=%s WHERE hostname=%s""", (online_at, hostname, ))
        mydb.commit()
        
        mydb.close()
        
    result = {
        "status": "200"
        }
    return result

##################### VERSION 2.0 METHOD #####################

def getBalance(pubkey):

    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    try:
        mycursor.execute("USE global")
        mycursor.execute("SELECT SUM(amount) AS received FROM transactions WHERE verified_at IS NOT NULL and receiver_pubkey LIKE '"+pubkey+"'")
        result = mycursor.fetchall()
        for item in result: 
            received = float(item[0])
    except:
        received = float(0)
    print ("received", received)
        
    try:
        mycursor.execute("USE global")
        mycursor.execute("SELECT SUM(amount) AS sent FROM transactions WHERE verified_at IS NOT NULL and sender_pubkey LIKE '"+pubkey+"'")
        result = mycursor.fetchall()
        for item in result: 
            sent = float(item[0])
    except:
        sent = float(0)
        
    balance = received - sent
    print ("balance", balance)
        
    data = {
        "coin_name": "Eulium",
        "coin_unit": "ELM",
        "received": received,
        "sent": sent,
        "current_balance": balance
        }
    
    result = {
        "data": data,
        "status": "200"
        }
    
    mydb.close()
    return result

##################### VERSION 2.0 METHOD #####################

def verifyTransaction():

    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()

    start_time = time.time()
    mycursor.execute("USE global")
    mycursor.execute("SELECT hash, certificate, sender_pubkey, receiver_pubkey, amount, gas_amount, coin_name, coin_unit, difficulty, created_at, verified_at, execution_time, rejected_at, hash_block FROM transactions WHERE verified_at IS NULL and rejected_at IS NULL ORDER BY created_at ASC LIMIT 1") 
    result = mycursor.fetchall()
    
    mydb.close()
    
    if not result:
        print ('No transactions to verify')
        return
    else:
        for item in result:
            hash_masked = item[0]
            certificate = item[1]
            sender_pubkey = item[2]
            receiver_pubkey = item[3]
            amount = item[4]
            gas_amount = item[5]
            coin_name = item[6]
            coin_unit = item[7]
            difficulty = item[8]
            created_at = item[9]
            verified_at = item[10]
            execution_time = item[11]
            rejected_at = item[12]
            hash_block = item[13]

            your_list = "0123456789"
            complete_list = []
            for current in range(40):
                a = [i for i in your_list]
                for y in range(current):
                    a = [x+i for i in your_list for x in a]
                complete_list = complete_list + a
                for mask in complete_list:
                    hash = hash_masked + mask
                    print ("Candidate_hash: ", hash)
                    status = validateHashCertificate(certificate, hash)
                    #print ("status validateHashCertificate", status)
                    if (status == True):
                        print ('hash', hash)
                        print ('certificate', certificate)
                        print ('sender_pubkey', sender_pubkey)
                        print ('receiver_pubkey', receiver_pubkey)
                        print ('amount', amount)
                        print ('gas_amount', gas_amount)
                        print ('coin_name', coin_name)
                        print ('coin_unit', coin_unit)
                        print ('difficulty', difficulty)
                        print ('created_at', created_at)
                        print ('verified_at', verified_at)
                        print ('execution_time', execution_time)
                        print ('rejected_at', rejected_at)
                        print ('hash_block', hash_block)

                        #FINAL CHECK SENDER WALLET BALANCE
                        getBalance_json = getBalance(sender_pubkey)
                        data_json = getBalance_json['data']
                        balance = data_json['current_balance']
                        print ("current_balance", balance)
                        
                        amount_total = amount + gas_amount
                        print ('Beginning balance: ', balance)
                        print ('Transaction amount + gas: ', amount_total)

                        #CHECK BALANCE TO VERIFY TRANSACTION (IF IT'S POP REWARD IT ALSO MUST BE APPROVED)
                        if balance >= amount_total or sender_pubkey == "REWARD":
                            
                            mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
                            mycursor = mydb.cursor()
     
                            verified_at = round(time.time() * 1000)
                            print ("verified_at", verified_at)
                            execution_time = (time.time() - start_time)
                            print ("execution_time", execution_time)
                            
                            mycursor.execute("USE global")
                            mycursor.execute("""UPDATE transactions SET hash=%s, execution_time=%s, verified_at=%s WHERE certificate=%s and created_at=%s""", (hash, execution_time, verified_at, certificate, created_at, ))
                            mydb.commit()

                            print ("SUCCESS: Transaction verified")
                            
                            mydb.close()
                            return
                
                        else:
                            
                            mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
                            mycursor = mydb.cursor()
                            
                            rejected_at = round(time.time() * 1000)
                            print ("rejected_at", rejected_at)
                            execution_time = (time.time() - start_time)
                            print ("execution_time", execution_time)
                            
                            mycursor.execute("USE global")
                            mycursor.execute("""UPDATE transactions SET hash=%s, gas_amount=%s, execution_time=%s, rejected_at=%s, coin_name=%s, coin_unit=%s WHERE certificate=%s and created_at=%s""", (hash, gas_amount, execution_time, rejected_at, coin_name, coin_unit, certificate, created_at, ))
                            mydb.commit()
                            
                            print ("REJECTED : Not enough funds")
                            
                            mydb.close()
                            return
                    else:
                        pass

##################### VERSION 2.0 METHOD #####################

def validateHashCertificate(certificate, hash):
    try:
        password, salt = certificate.split(':')
        return password == hashlib.sha256(salt.encode() + hash.encode()).hexdigest()
    except:
        return False
    
##################### VERSION 2.0 METHOD #####################
    
def mineBlock():

    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    count = 0
    verified_transactions_dict = {}
    meta = {}
    reward = []
    
    mycursor.execute("USE global")
    mycursor.execute("SELECT hash, certificate, sender_pubkey, receiver_pubkey, amount, gas_amount, coin_name, coin_unit, difficulty, created_at FROM transactions WHERE hash_block IS NULL and verified_at IS NOT NULL")
    result = mycursor.fetchall()
    
    mydb.close()
    
    for item in result:
        transaction_hash = item[0]
        transaction_certificate = item[1]
        
        meta_hash = item[0]
        meta_certificate = item[1]
        meta_sender_pubkey = item[2]
        meta_receiver_pubkey = item[3]
        meta_amount = item[4]
        meta_gas_amount = item[5]
        meta_coin_name = item[6]
        meta_coin_unit = item[7]
        meta_difficulty = item[8]
        meta_created_at = item[9]
        
        meta_data = {
            "hash": meta_hash,
            "certificate": meta_certificate,
            "sender_pubkey": meta_sender_pubkey,
            "receiver_pubkey": meta_receiver_pubkey,
            "amount": meta_amount,
            "gas_amount": meta_gas_amount,
            "coin_name": meta_coin_name,
            "coin_unit": meta_coin_unit,
            "difficulty": meta_difficulty,
            "created_at": meta_created_at
            }
        
        #COLLECT AMOUNTS OF MINING TRANSACTIONS TO CALCULATE COINS_LEFT IN BLOCK
        if meta_sender_pubkey == 'REWARD':
            reward.append(meta_amount)
  
        #COLLECT HASH AND CERTIFICATE OF NON VERIFIED TRANSACTIONS IN DICT
        verified_transactions_dict.update({ transaction_hash : transaction_certificate })

        count = count + 1
        
        #COLLECTING META IN DICT FOR HASH BLOCK
        meta.update({ count : meta_data })
        
        if count == 5:
            break
    
    #EACH BLOCK MUST CONTAIN FIVE TRANSACTIONS
    if count < 5:
        print ("Not enough verified transactions to create a new block")
        return
    
    #AS SOON AS SET OF FIVE VALIDATED TRANSACTIONS COLLECTED THE MAIN LOGIC STARTED
    print ("verified_transactions_dict", verified_transactions_dict)
    
    #GENERATE HASH BLOCK BASED ON META DATA
    hash_block = generateHashBlock(meta)
    print ("hash_block", hash_block)
    
    for transaction_hash, transaction_certificate in verified_transactions_dict.items():
        
        #UPDATE TRANSACTION VIA ADDING HASH OF FUTURE BLOCK
        try:
            mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
            mycursor = mydb.cursor()

            mycursor.execute("USE global")
            mycursor.execute("""UPDATE transactions SET hash_block=%s WHERE hash=%s AND certificate=%s""", (hash_block, transaction_hash, transaction_certificate, ))
            mydb.commit()
            mydb.close()
            
            print ("SUCCESS: transaction updated on local node", transaction_hash)
        except:
            print ("ERROR")
        
    #GET PREVIOUS BLOCK DATA FROM LOCAL NODE
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()

    mycursor.execute("USE global")
    mycursor.execute("SELECT hash, coins_left FROM blocks WHERE pre_hash IS NOT NULL ORDER BY created_at DESC LIMIT 1")
    result = mycursor.fetchall()
    for item in result:
        pre_hash = item[0]
        pre_coins_left = item[1]
        
    mydb.close()
        
    #ADD NEW BLOCK TO LOCAL NODE
    created_at = round(time.time() * 1000)
    meta = str(meta)
    
    #CALCULATE COIUNS LEFT FOR NEW BLOCK BASED ON SUM OF REWARDS AMOUNT
    if reward:
        sum_reward = sum(reward)
    else:
        sum_reward = float(0)
    coins_left = pre_coins_left - sum_reward
    if coins_left <= 0:
        coins_left = float(0)

    try:    
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        sql = "INSERT INTO blocks (hash, pre_hash, meta, coins_left, created_at) VALUES (%s, %s, %s, %s, %s)"
        val = (hash_block, pre_hash, meta, coins_left, created_at)
        mycursor.execute("USE global")
        mycursor.execute(sql, val)
        mydb.commit()
        mydb.close()
        
        print ("SUCCESS: new block has been added to local node")
    except:
        print ("ERROR")
    
    return

##################### VERSION 2.0 METHOD #####################
    
def getRewardWalletBalance():
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    mycursor.execute("USE local")
    mycursor.execute("SELECT reward_pubkey from settings")
    result = mycursor.fetchall()
     
    for item in result:
        reward_pubkey = item[0]
        print ("reward_pubkey", reward_pubkey)
        
    result = getBalance(reward_pubkey)
    
    mydb.close()
    return result

##################### VERSION 2.0 METHOD #####################
    
def getRewardWalletPubkey():
    
    mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
    mycursor = mydb.cursor()
    
    mycursor.execute("USE local")
    mycursor.execute("SELECT reward_pubkey from settings")
    result = mycursor.fetchall()
    
    for item in result:
        pubkey = item[0]
        
    data = {
        "pubkey": pubkey 
        }

    result = {
        "data": data,
        "status": "200"
        }
    
    mydb.close()
    return result

##################### VERSION 2.0 METHOD #####################

def generateHashCertificatePair(difficulty):
    letters = "0123456789"
    hash_size = 40
    open = hash_size - difficulty
    hash_original = ( ''.join(random.choice(letters) for i in range(hash_size)) )
    print ('hash_original', hash_original)
    hash = hash_original[:open]
    # uuid is used to generate a random number
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + hash_original.encode()).hexdigest() + ':' + salt, hash

##################### VERSION 2.0 METHOD #####################

def postHashBlock(transaction_hash, transaction_certificate, hash_block):
    
    try:
        print ("transaction_hash", transaction_hash)
        print ("transaction_certificate", transaction_certificate)
        print ("hash_block", hash_block)
        
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()

        mycursor.execute("USE global")
        mycursor.execute("""UPDATE transactions SET hash_block=%s WHERE hash=%s AND certificate=%s""", (hash_block, transaction_hash, transaction_certificate, ))
        mydb.commit()
        
        mydb.close()
        
        result = {
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result

##################### VERSION 2.0 METHOD #####################

def postBlock(hash, pre_hash, meta, coins_left, created_at):
    try:
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()

        sql = "INSERT INTO blocks (hash, pre_hash, meta, coins_left, created_at) VALUES (%s, %s, %s, %s, %s)"
        val = (hash, pre_hash, meta, coins_left, created_at)
        mycursor.execute("USE global")
        mycursor.execute(sql, val)
        mydb.commit()
        
        mydb.close()
        
        result = {
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result

##################### VERSION 2.0 METHOD #####################

def updateOnlineStatus(hostname):
    
    try:
        mydb = mysql.connector.connect(host=host, user=user, password=password, database=db)
        mycursor = mydb.cursor()
        
        online_at = round(time.time() * 1000)
        
        mycursor.execute("USE global")
        mycursor.execute("""UPDATE nodes SET online_at=%s WHERE hostname=%s""", (online_at, hostname, ))
        mydb.commit()
        
        mydb.close()
    
        result = {
            "status": "200"
            }
    except:
        result = {
            "status": "404"
            }
    return result

##################### VERSION 2.0 METHOD #####################

def generateHashBlock(meta):
    
    meta = str(meta)
    hash_object = hashlib.sha256(str(meta).encode('utf-8'))
    value = hash_object.hexdigest()
    
    return value
    
##################### VERSION 2.0 METHOD #####################

def mineReward():
    
    #POST PROOF OF POWER REWARD TRANSACTION TO ALL AVAILABLE NODES
    daily_reward_pool = float(10000)
    
    difficulty = 5
    certificate_pop_reward, hash_pop_reward = generateHashCertificatePair(difficulty)
    try:
        receiver_pubkey = getRewardWalletPubkey()['data']
        reward_pubkey = receiver_pubkey['pubkey']  
    except:
        print ("Reward key not assigned")
        return
      
    coin_name = "Eulium"
    coin_unit = "ELM"
    gas_amount = float(0)
    sender_pubkey = "REWARD"
    created_at = round(time.time() * 1000)
    
    result = getNodes()
    
    active_nodes_count = len(result)
    reward_amount = daily_reward_pool / active_nodes_count
    
    for item in result:
        hostname = item[0]
        url = "http://"+hostname+"/postTransaction"
        data = {'hash': hash_pop_reward, 'certificate': certificate_pop_reward, 'sender_pubkey': sender_pubkey, 'receiver_pubkey': reward_pubkey, 'amount': reward_amount, 'gas_amount': gas_amount, 'coin_name': coin_name, 'coin_unit': coin_unit, 'difficulty': difficulty, 'created_at': created_at}
        try:
            requests.post(url, data, timeout=5)
            print(hostname, "Reward transaction successfully posted to", url)
        except:
            print(hostname, "Can't open URL", url)

    return

##################### VERSION 2.0 METHOD #####################