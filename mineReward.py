import schedule
import time
import datetime

from api import mineReward

now = datetime.datetime.now()
minute = int(now.minute)
if minute == 0:
    minute = 59
else:
    minute = minute - 1
hour = str(now.hour)
minute = str (minute)
if len(str(hour)) == 1:
    hour = "0" + hour
if len(str(minute)) == 1:
    hour = "0" + hour

activation_time = hour + ":" + minute 
print ("activation_time", activation_time)

schedule.every().day.at(activation_time).do(mineReward)

while True:
    try:
        #LISTENING TIME WHEN REWARD WILL BE POSTED (ONCE A DAY, EVERYDAY)
        schedule.run_pending()
    except:
        pass
    time.sleep(60)