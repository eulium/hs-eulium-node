import shutil
import os

from api import syncLocalDB
from api import syncGlobalDB

import subprocess
from subprocess import Popen, PIPE

# Description: Set up MySQL Databases
syncLocalDB()
syncGlobalDB()

# Description: Cereate startup folder and copy startup scripts into (not affected via software update)
try:
    os.system("sudo rm -rf /hs-eulium-node-startup")
except:
    pass
os.system("sudo mkdir /hs-eulium-node-startup")
os.system("sudo chmod -R 777 /hs-eulium-node-startup/")
os.system("sudo chmod -R 777 /hs-eulium-node/")
shutil.copy2('/hs-eulium-node/startup.py', '/hs-eulium-node-startup/startup.py')
shutil.copy2('/hs-eulium-node/updateSoftware.py', '/hs-eulium-node-startup/updateSoftware.py')